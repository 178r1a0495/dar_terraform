# configured aws provider with proper credentials
provider "aws" {
  region     = "us-east-2"
  access_key = "AKIAUAIYJYD6AX6DYN5K"
  secret_key = "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"
}

# create default vpc if one does not exit
resource "aws_default_vpc" "DAR_SG" {
  
  tags = {
    Name  = "DAR_SG" 
  }
}

# use data source to get all avalability zones in region
data "aws_availability_zones" "available_zones" {}

# create default subnets if one does not exit
resource "aws_default_subnet" "default_az1" {
   availability_zone = data.aws_availability_zones.available_zones.names[0]

    tags = {
      Name = "DAR_subnet"
    }
}


# create security group for the ec2 instance
resource "aws_security_group" "DAR_SG" {
  name        = "DAR_SG"
  description = "allow access on ports 80 and 22"
  vpc_id      = "vpc-01609f62e104aadd8"

  ingress {
    description      = "http access"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "ssh access"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["103.186.234.3/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = -1
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags   = {
    Name = "DAR_SG"
  }
}

# use data source to get a registered amazon linux 2 ami
data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]
  
  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}


# launch the ec2 instance and install website
resource "aws_instance" "ec2_instance" {
  ami                    = "ami-024e6efaf93d85776"
  instance_type          = "t2.micro"
  subnet_id              = aws_default_subnet.default_az1.id
  vpc_security_group_ids = [aws_security_group.DAR_SG.id]
  key_name               = "Terraform"

  tags = {
    Name = "DAR_Terraform"
  }
}


# print the ec2's public ipv4 address
output "public_ipv4_address" {
  value = aws_instance.ec2_instance.public_ip
}

output "private_ipv6_address" {
    value = aws_instance.ec2_instance.private_ip
}